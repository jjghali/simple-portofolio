var request = require("request-promise");

var options = {
    method: 'GET',
    url: '',
    headers:
    {
        'cache-control': 'no-cache',
        Accept: 'application/vnd.github.v3+json',
        'User-Agent': 'https://api.github.com/meta'
    }
};




const getRepos = (apiUrl) => {
    options.url = apiUrl;

    return request(options, function (error, response, body) {
        if (error) throw new Error(error);
    }).then((body) => {
        return body
    });

}

module.exports = { getRepos: getRepos };