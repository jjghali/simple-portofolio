const Gitlab = require('./gitlab');
const Github = require('./github');
// let projects = [];


const getRepos = (res, profile) => {
    let gitlabPromise = Gitlab.getRepos(profile.gitlabApi);

    let promise = gitlabPromise.then((resolve) => {

        let proj = [];
        resolve.forEach(element => {
            let project = {};
            project.title = element.name;
            project.url = element.web_url;
            project.provider = "gitlab";
            project.description = element.description;
            proj.push(project);
        });

        return proj;
    }).then((gitlabProjects) => {
        Github.getRepos(profile.githubApi).then((ghProjects) => {
            let arrayPr = JSON.parse(ghProjects);
            arrayPr.forEach(element => {
                if (gitlabProjects.filter(p => p.title == element.title).length == 0 && !element.fork) {
                    let project = {};
                    project.title = element.name;
                    project.url = element.html_url;
                    project.provider = "github";
                    project.description = element.description;
                    gitlabProjects.push(project);
                }
            });
            return gitlabProjects;
        }).then((projects => {
            res.render('index', { projects: projects, profile: profile });
        }));
    });
};


module.exports = {
    getRepos: getRepos
};