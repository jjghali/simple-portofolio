var request = require("request-promise");

var options = {
    method: 'GET',
    url: '',
    qs: { visibility: 'public' },
    headers:
    {
        'Content-Type': 'application/json'
    },
    body: { visibility: 'public' },
    json: true
};

const getRepos = (apiUrl) => {
    options.url = apiUrl;

    return request(options, function (error, response, body) {
        if (error) throw new Error(error);
    }).then((body) => {
        // console.log(JSON.stringify(body));
        return body;
    });
}


module.exports = { getRepos: getRepos }