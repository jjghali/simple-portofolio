const Gravatar = require('gravatar-url');
const YAML = require('yaml');
const fs = require('fs');

const profileFile = fs.readFileSync('profile.yml', 'utf8');

const getProfile = () => {
    let profile = YAML.parse(profileFile);
    console.log(JSON.stringify(profile));
    profile.gravatarUrl = Gravatar(profile.gravatarEmail, { size: 200 });

    return profile;
}

module.exports = {
    getProfile: getProfile
};